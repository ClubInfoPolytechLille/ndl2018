Utilisation de l'appli androïd

Cette application est l'appli compagnon de l'explorateur. Elle doit lui permettre d'envoyer des messages et des status pour les viewers.

*L'application peut être modifiée à l'aide d'APP inventor à l'aide de l'archive de projet .aia
*Installer le .apk sur le téléphone cible

L'application possède plusieurs fonctionnalités :
*Elles envoie des données collectées sur le téléphone du chercheur vers le serveur principal
**Coordonées
**Altitude
**Détection de chute du téléphone et peut être de son proprio, mais RHINOSHIEEEEEEEEEEEEEELD donc ça va

*Elle permet au chercheur d'accéder à plusieurs outils :
**Une carte pour se repérer dans le désert
**Une interface pour envoyer un message au live ou actualiser son statut
**Une interface de configuration qui permet aussi de visualiser la trame envoyée au server.
