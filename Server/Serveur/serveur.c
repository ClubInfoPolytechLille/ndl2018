/** Fichier serveur.c **/

/***********************************************************/
/** Serveur pour le serveur                               **/
/***********************************************************/

/** Fichiers d'inclusion **/

#ifndef SERVEUR_H
#define SERVEUR_H
#include "serveur.h"
#endif

/** Variables publiques **/

extern capteur_t valInstantanees[NBBINOME];
extern int socket_udp;

/** Variables statiques **/

char port[100] = "55555";

/** Procedure principale **/

void my_delay(int i)    /*Pause l'application pour i seconds*/
{
    clock_t start,end;
    start=clock();
    while(((end=clock())-start)<=i*CLOCKS_PER_SEC);
}

void BonneSyntaxe(){
	printf("Parametre incorrect, syntaxe : -p <num_port> ou --port <num_port>\n");
}

void analyseArguments(int argc,char **argv){

	static struct option long_options[] = {
		{"port", required_argument, 0, 'p'},
    };

    int option_index = 0;
  	int c;

    while ((c = getopt_long (argc, argv, "p:",long_options, &option_index)) != -1){

	    switch(c){

	        case 'p':
	         	strcpy(port,optarg);
	          	break;

	        default:
	     		BonneSyntaxe();
	         	break;

        }
	}
}

int fonction_thread(int s){

	return lanceThread((void *)gestionClient,&s,sizeof(s));
}

void init_structure()
{
	FILE * fichier = NULL;
	char * name = "data.json";
	int fd;

	fd = open(name, O_CREAT | O_EXCL , 0644);

	if (fd > 0)
	{
		printf("*** Creation du fichier %s *** \n", name);
		close(fd);
		fichier = fopen(name, "w");

		fprintf(fichier, "{ \"tableau\":[\n" );
		fprintf(fichier, "]}" );

		fclose(fichier);
	}else
	{
		printf("Fichier %s déjà existant\n", name);
	}


}

int main(int argc,char **argv){

	//int s;
	  
	/* Lecture des arguments de la commande */
	analyseArguments(argc,argv);

	/* Initialisation du serveur UDP*/
	init_structure();
	lanceThread(serveurUDP_initialisation,NULL,0);
	
	/* Initialisation du serveur TCP */
	/*s=initialisationServeur(port,MAX_CONNEXIONS);*/
	 
	/* Lancement de la boucle de traitement des clients */
	//printf("** Lancement de la boucle de traitement des clients sur la socket %d **\n",s);
	/*if (boucleServeur(s,fonction_thread) == -1){
		printf("Fail connexion\n");
	};*/

	while(1);

	printf("Fin de la boucle d'ecoute\n");
	
	return(0);
}
