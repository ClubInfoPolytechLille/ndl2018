/** fichier serveur.h **/

#include <stdint.h>
#include <inttypes.h>
#ifndef LIBCOM_H
#define LIBCOM_H
#include "libcom.h"
#endif

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>


#ifndef LIBCOM_H
#define LIBCOM_H
#include "libcom.h"
#endif

#include "libthrd.h"

#ifndef HTTP_H
#define HTTP_H
#include "http.h"
#endif

#ifndef CAPTEUR_H
#define CAPTEUR_H
#include "capteur.h"
#endif

/*********************************************/
/* Structures necessaires pour le serveur    */
/*********************************************/

/** Constantes **/

/* Nombre de binomes max */
#define NBBINOME 256

/** Structures **/

/******************************************************************************************************/
/* capteur_s : Structure pour stockage des valeurs instantannées */
/* estConnu : est à 1 si capteur déjà répertorié, 0 sinon */
/* x,y,z : coordonnées de l'acceleromètre */
/* temp : température */
/* id : id du groupe */
/******************************************************************************************************/

typedef struct capteur_s 
{ 
    uint8_t estConnu;
	float x,y,z;
    float temp;
    uint8_t id;
} capteur_t;

/** Variables publiques  **/

/******************************************************************************************************/
/* tableau de structure stockant les valeurs instantannées */
/******************************************************************************************************/

capteur_t valInstantanees[NBBINOME];

/******************************************************************************************************/
/* init_structure : Fonction d'initialisation de la structure */
/* Au lancement du serveur, toutes les valeurs ont réinitialisé à 0 */
/******************************************************************************************************/

void init_structure();