
#include "http.h"

extern capteur_t valInstantanees[NBBINOME];

void gestionClient(void * arg){

	int s = *(int*)arg;
	char ligne[MAX_LIGNE];
	/* Obtient une structure de fichier */
	FILE *dialogue=fdopen(s,"a+");
	if(dialogue==NULL){ perror("gestionClient.fdopen"); exit(EXIT_FAILURE); }

	fgets(ligne,MAX_LIGNE,dialogue);

	if(strcmp(ligne, "GET / HTTP/1.1\r\n") == 0)
	{
		indexhtml(dialogue);
	}
	else if(strcmp(ligne, "GET /graphes.html HTTP/1.1\r\n")==0)
	{
		grapheshtml(dialogue);
	}
	else if(strcmp(ligne, "GET /valeurs.html HTTP/1.1\r\n")==0)
	{
		valeurshtml(dialogue);
	}

	

	/* Termine la connexion */
	printf("Fermeture connexion\n");

	fclose(dialogue);
}

void indexhtml(FILE * dialogue)
{
	headerHtml(dialogue);

	fprintf(dialogue, "Veuillez sélectionner la page que vous souhaitez afficher<br>" );

	fprintf(dialogue, "<a href=\"graphes.html\">Graphes temporels des capteurs</a><br>" );

	fprintf(dialogue, "<a href=\"valeurs.html\">Valeurs instantanées des capteurs</a><br>" );

	footerHtml(dialogue);
}

void grapheshtml(FILE * dialogue)
{
	int state = 0;
	int nbline;
	int longueur;
	FILE * noms;
	FILE * data;
	char strNom[4];
	char strData[100];
	float dataFile;
	int xX,xY,xZ,xT,y;

	headerHtml(dialogue);

	P(666);
	noms = fopen("noms", "r");

	if(noms != NULL)
	{
		while(fgets(strNom, 4, noms)!=NULL)
		{
			xX = xY = xZ = xT = 0;
			P(atoi(strNom));

			int cpt = 0;
			while (strNom[cpt] != '\n'){
				cpt++;
			}
			strNom[cpt] = '\0';

			nbline = countNbLineFile(strNom);

			longueur = (nbline/5) * PIXPERVAL;
			y = HAUTEURPIX/2;
			

			fprintf(dialogue, "Capteur %s<br>", strNom );
			fprintf(dialogue, "axe x : <canvas id=\"canvasx%s\" width=\"%d\" height=\"%d\" style=\"border:1px solid #000000;\">Canvas non supporté par le navigateur</canvas><br>", strNom, longueur, HAUTEURPIX );
			fprintf(dialogue, "axe y : <canvas id=\"canvasy%s\" width=\"%d\" height=\"%d\" style=\"border:1px solid #000000;\">Canvas non supporté par le navigateur</canvas><br>", strNom, longueur, HAUTEURPIX );
			fprintf(dialogue, "axe z : <canvas id=\"canvasz%s\" width=\"%d\" height=\"%d\" style=\"border:1px solid #000000;\">Canvas non supporté par le navigateur</canvas><br>", strNom, longueur, HAUTEURPIX );
			fprintf(dialogue, "température : <canvas id=\"canvast%s\" width=\"%d\" height=\"%d\" style=\"border:1px solid #000000;\">Canvas non supporté par le navigateur</canvas><br>", strNom, longueur, HAUTEURPIX );
			
			data = fopen(strNom, "r");

			fprintf(dialogue, "<script>" );

			fprintf(dialogue, "var canvasx = document.getElementById(\"canvasx%s\");var ctxX = canvasx.getContext(\"2d\");",
							   strNom);

			fprintf(dialogue, "var canvasy = document.getElementById(\"canvasy%s\");var ctxY = canvasy.getContext(\"2d\");",
							   strNom);

			fprintf(dialogue, "var canvasz = document.getElementById(\"canvasz%s\");var ctxZ = canvasz.getContext(\"2d\");",
							   strNom);

			fprintf(dialogue, "var canvast = document.getElementById(\"canvast%s\");var ctxT = canvast.getContext(\"2d\");",
							   strNom);

			fprintf(dialogue, "ctxX.moveTo(%d, %d);", xX, y );
			fprintf(dialogue, "ctxY.moveTo(%d, %d);", xY, y );
			fprintf(dialogue, "ctxZ.moveTo(%d, %d);", xZ, y );
			fprintf(dialogue, "ctxT.moveTo(%d, %d);", xT, y );

			while(fgets(strData, 61, data)!=NULL)
			{

				switch(state)
				{
					case 0:
						state = 1;break;
					case 1:
						dataFile = atof(strData);
						y =  HAUTEURPIX/2 - dataFile*PIXPER1G;
						xX+=PIXPERVAL;
						fprintf(dialogue, "ctxX.lineTo(%d, %d);", xX, y );
						fprintf(dialogue, "ctxX.stroke();");
						fprintf(dialogue, "ctxX.moveTo(%d, %d);", xX, y );

						state = 2; break;
					case 2:
						dataFile = atof(strData);
						y = HAUTEURPIX/2 - dataFile*PIXPER1G;
						xY+=PIXPERVAL;
						fprintf(dialogue, "ctxY.lineTo(%d, %d);", xY, y );
						fprintf(dialogue, "ctxY.stroke();");
						fprintf(dialogue, "ctxY.moveTo(%d, %d);", xY, y );
						state = 3;break;
					case 3:
						dataFile = atof(strData);
						y = HAUTEURPIX/2 - dataFile*PIXPER1G;
						xZ+=PIXPERVAL;
						fprintf(dialogue, "ctxZ.lineTo(%d, %d);", xZ, y );
						fprintf(dialogue, "ctxZ.stroke();");
						fprintf(dialogue, "ctxZ.moveTo(%d, %d);", xZ, y );
						state = 4;break;
					case 4:
						dataFile = atof(strData);
						y =  HAUTEURPIX/2 - dataFile*PIXPER1D;
						xT+=PIXPERVAL;
						fprintf(dialogue, "ctxT.lineTo(%d, %d);", xT, y );
						fprintf(dialogue, "ctxT.stroke();");
						fprintf(dialogue, "ctxT.moveTo(%d, %d);", xT, y );
						state = 0;break;
					default: break;
				}
				
			}

			fprintf(dialogue, "</script>" );

			fclose(data);
			V(atoi(strNom));
		}


		fclose(noms);
		
	}else
	{
		fprintf(dialogue, "--------------------------<br>");
		fprintf(dialogue, "Désolé, aucun capteur n'a été détecté !<br>");
		fprintf(dialogue, "--------------------------<br>");
	}
	footerHtml(dialogue);
	V(666);
}


void valeurshtml(FILE * dialogue)
{
	int cpt;
	char auMoinsUnCapteurConnu = 0;
	
	headerHtml(dialogue);

	fprintf(dialogue, "Les valeurs instantanées des capteurs sont : <br>");

	for(cpt = 0; cpt < NBBINOME; cpt++)
	{
		if(valInstantanees[cpt].estConnu)
		{
			auMoinsUnCapteurConnu = 1;
			fprintf(dialogue, "--------------------------<br>");
			fprintf(dialogue, "Capteur %d : ", cpt);
			fprintf(dialogue, "<br>" );
			fprintf(dialogue, "x : %f\n", valInstantanees[cpt].x );
			fprintf(dialogue, "<br>" );
			fprintf(dialogue, "y : %f\n", valInstantanees[cpt].y );
			fprintf(dialogue, "<br>" );
			fprintf(dialogue, "z : %f\n", valInstantanees[cpt].z );
			fprintf(dialogue, "<br>" );
			fprintf(dialogue, "temp : %f\n", valInstantanees[cpt].temp );
			fprintf(dialogue, "<br>" );
		}
	}

	if(!auMoinsUnCapteurConnu)
	{
		fprintf(dialogue, "--------------------------<br>");
		fprintf(dialogue, "Désolé, aucun capteur n'a été détecté !<br>");
		fprintf(dialogue, "--------------------------<br>");
	}
	footerHtml(dialogue);
}

int countNbLineFile(const char *filename)
{
	int cpt = 0;
	char strData[60];
	FILE * data;

	data = fopen(filename, "r");

	if (data == NULL)
    {
    	printf("%s\n",filename);
    	perror("countNbLineFile.fopen"); exit(EXIT_FAILURE);
    }
 
    
	while(fgets(strData, 61, data)!=NULL)
	{
        cpt = cpt + 1;
	}
 
    fclose(data);

    return cpt;

}

void headerHtml(FILE * dialogue)
{

	time_t rawtime;
    struct tm *info;
	char dayname[5];
	char day[5];
	char month[5];
	char year[5];
	char hour[5];
	char minute[5];
	char seconde[5]; 
	time( &rawtime );
    info = localtime( &rawtime );

    strftime(dayname,5,"%a", info);
    strftime(day,5,"%d", info);
    strftime(month,5,"%m", info);
    strftime(year,5,"%Y", info);
    strftime(hour,5,"%H", info);
    strftime(minute,5,"%M", info);
    strftime(seconde,5,"%S", info);


    fprintf(dialogue, "HTTP/1.1 200 OK\r\n" );
	fprintf(dialogue, "Server: IMA4SC/14 (Unix)\r\n" );
	fprintf(dialogue, "Date: %s, %s %s %s %s:%s:%s GMT\r\n", dayname, day, month, year, hour, minute, seconde );
	fprintf(dialogue, "Content-Type: text/html; charset=ANSI\r\n\r\n" );
	fprintf(dialogue, "<!DOCTYPE html PUBLIC \"-//IETF//DTD HTML 2.0//FR\"><html>" );
	fprintf(dialogue, "<body>" );
}

void footerHtml(FILE * dialogue)
{
	fprintf(dialogue, "</body>" );
	fprintf(dialogue, "</html>");
}
