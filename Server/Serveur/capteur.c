
#ifndef CAPTEUR_H
#define CAPTEUR_H
#include "capteur.h"
#endif

/* Importation du taleau stockant les valeurs instantannées */
extern capteur_t valInstantanees[NBBINOME];
extern char port[100];

pthread_mutex_t mutexFichier_json, mutexFichier_tmp;

char * JSONfileName = "data.json";
char * JSONTmpName = "tmp.json";

void wrapperLanceThread(unsigned char *paquet, int taille_paquet)
{    
    wrapperLanceThread_t wrappeur;
    wrappeur.paquet = malloc(taille_paquet);
    memset(wrappeur.paquet,'\0',taille_paquet);
    memcpy(wrappeur.paquet, paquet, taille_paquet);
    wrappeur.taille_paquet = taille_paquet;
    lanceThread(serveurUDP_lecture, (void*) &wrappeur , sizeof(wrappeur));
}

void serveurUDP_initialisation()
{
	printf("** initialisation serveur UDP **\n");
	serveurMessages(port,wrapperLanceThread);
}

void serveurUDP_lecture(void * arg)
{
    FILE * fichierJSON, *fichierTMP;
    int toAdd=0;  //count number lines in source file.
    int ch;   //temporary place to store character of source file(one at a time).
    int edited=0;  //0=false and 1=true
	printf("** serveurUDP_lecture **\n");
	wrapperLanceThread_t * param = (wrapperLanceThread_t *)arg;
    char command[100];

    printf("***Message reçu***\n");

    printf("%s\n", param->paquet);

    printf("***Fin du message***\n");

    printf("le caratere a la case %d est %c\n", param->taille_paquet, param->paquet[param->taille_paquet-1]);

    printf("Tentative de nettoyage\n");

    param->paquet[param->taille_paquet] = '\0';

    printf("nettoyage accompli\n");

        printf("***Message reçu nettoyé***\n");

    printf("%s\n", param->paquet);

    printf("***Fin du message***\n");

pthread_mutex_lock(&mutexFichier_tmp);
pthread_mutex_lock(&mutexFichier_json);
    fichierJSON = fopen(JSONfileName,"r");

    if (fichierJSON == NULL)
    {
        printf("pb ouverture fichier valeurs\n");
    }
    else
    {
        fichierTMP=fopen(JSONTmpName,"w");

        if(fichierJSON==NULL||fichierTMP==NULL)
        {
            printf("\nError...cannot open/create files");
        }else {

        while((ch=fgetc(fichierJSON))!=EOF)
        {
            if(ch=='\n')  //counts number of lines
                toAdd++;

            fprintf(fichierTMP,"%c",ch);

            if(toAdd && !edited)
            {
                fprintf(fichierTMP,"%s",param->paquet);
                fprintf(fichierTMP,",");
                edited++;
            }
        }}
        fclose(fichierJSON);
        fclose(fichierTMP);
   
    }
pthread_mutex_unlock(&mutexFichier_json);
pthread_mutex_unlock(&mutexFichier_tmp);

memset(command, 0, 100);
strcpy(command, "sudo mv ");
strcat(command, JSONTmpName);
strcat(command, " ");
strcat(command, JSONfileName);

system(command);

memset(param->paquet, 0, param->taille_paquet);
    free(param->paquet);

}
