#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <time.h>

#ifndef SERVEUR_H
#define SERVEUR_H
#include "serveur.h"
#endif


#include "libthrd.h"

/* Taille max des lignes du fichier socket */
#define TAILLE_MAX 1000

/* Conctantes pour la génération du canvas */
#define PIXPERVAL 200
#define HAUTEURPIX 100
#define PIXPER1G (HAUTEURPIX/8)
#define PIXPER1D 1

/******************************************************************************************************/
/* gestionClient: Fonction de gestion des client connecté au serveur TCP */
/* arg : pointeur vers la socket TCP */ 
/******************************************************************************************************/

void gestionClient(void * arg);


/******************************************************************************************************/
/* grapheshtml : Fonction de génération de la page d'affichage des graphes */
/* dialogue : pointeur vers l'ouverture de la socket */
/******************************************************************************************************/

void grapheshtml(FILE * dialogue);


/******************************************************************************************************/
/* valeurshtml : Fonction de génération de la page d'affichage des valeurs instantannées */
/* dialogue : pointeur vers l'ouverture de la socket */
/******************************************************************************************************/

void valeurshtml(FILE * dialogue);


/******************************************************************************************************/
/* countNbLineFile : Compte le nombre de ligne dans un fichier de capteur afin de générer le canvas des graphes*/
/* filename : pointeur vers l'ouverture d'un fichier binaire de capteur*/
/******************************************************************************************************/

int countNbLineFile(const char *filename);


/******************************************************************************************************/
/* indexhtml : fonction de génération de la page d'acceuil du server web */
/* dialogue : pointeur vers l'ouverture de la socket */
/******************************************************************************************************/

void indexhtml(FILE * dialogue);


/******************************************************************************************************/
/* headerHtml : génération d'une en-tête html */
/* dialogue : pointeur vers l'ouverture de la socket */
/******************************************************************************************************/

void headerHtml(FILE * dialogue);


/******************************************************************************************************/
/* footerHtml : génération de balises fermantes html */
/* dialogue : pointeur vers l'ouverture de la socket */
/******************************************************************************************************/

void footerHtml(FILE * dialogue);
