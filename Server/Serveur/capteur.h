#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>

#ifndef SERVEUR_H
#define SERVEUR_H
#include "serveur.h"
#endif

#ifndef LIBCOM_H
#define LIBCOM_H
#include "libcom.h"
#endif

/* Constantes pour calcul des valeurs reçu par les capteurs */

#define VCC 3.3
#define QUANTUM VCC/255.0

/******************************************************************************************************/
/* wrapperLanceThread_s: structure qui contient les paramètres de la fonction de traitement lancé par 'lanceThread' */
/* paquet : données UDP reçues qui seront traité par la fonction de traitement des paquets UDP */
/* taille_paquet : taille des données reçu en UDP */
/******************************************************************************************************/

typedef struct wrapperLanceThread_s {
    int taille_paquet;
    unsigned char * paquet;
} wrapperLanceThread_t;


/******************************************************************************************************/
/* wrapperLanceThread: wrapper pour appel à la fonction 'lanceThread' dans libthrd.c */
/* lance une fonction dont le pointeur est passé en paramètre via la fonction 'lanceThread' */
/* taille_paquet : pointe vers les données reçu en UDP */
/* nboctets : taille des données reçu en UDP */
/******************************************************************************************************/

void wrapperLanceThread(unsigned char *paquet, int taille_paquet);


/******************************************************************************************************/
/* serveurUDP_initialisation : fonction d'initialisation du serveur UDP, lance 'serveurMessages' */
/******************************************************************************************************/

void serveurUDP_initialisation();


/******************************************************************************************************/
/* serveurUDP_lecture : fonction de lecture des paquet UDP */
/* lit les données reçues en UDP et les converties en valeurs flottantes */
/* stocke la date et l'heure */
/* stocke les données reçues dans la liste contigue 'valInstantanees' */
/* écrit ces données dans des fichiers binaires */
/* détecte une éventuel chute */ 
/******************************************************************************************************/

void serveurUDP_lecture(void * arg);


/******************************************************************************************************/
/* valeurs_instantanees : Stocke les données reçu dans la liste contigue 'valInstantanees' */
/* x,y,z,temp,id : données reçues en UDP */
/******************************************************************************************************/

void valeurs_instantanees(float x, float y, float z, float temp, int id);


/******************************************************************************************************/
/* ecrire_noms : écrit id dans le fichier binaire noms */
/* vérifie préalablement si id n'a pas déjà été répértorié */  
/******************************************************************************************************/

void ecrire_noms(int id);


/******************************************************************************************************/
/* ecrire_valeurs: écrit les données passé en paramètre dans un fichier de nom id */
/* mois,jour,heure,minute,secondes : date et heure du système */
/* x,y,z,temp,id : données reçues converties en flottants ou en int */
/******************************************************************************************************/

void ecrire_valeurs(char * mois, char * jour, char * heure, char * minute, char * secondes, float x, float y, float z, float temp, char * id);


/******************************************************************************************************/
/* detection_chute : détecte une éventuel chute */
/* z_before : coordonée en z stocké dans la liste des valeurs instantanés */
/* z_after : nouvelle coordonée en z reçue dans les données UDP */
/* port : port sur lequel envoyer une message UDP en urgence si on détecte une chute */  
/******************************************************************************************************/

void detection_chute(float z_before, float z_after, char * id, char * port);


