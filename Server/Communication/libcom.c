/** fichier libcom.c **/

/*****************************************/
/** Ce fichier contient des fonctions  **/
/**  concernant les sockets.           **/
/****************************************/

/**** Fichiers d'inclusion ****/

#include "libcom.h"

/**** Variables globales *****/

int socket_udp;

/**** Fonctions de gestion des sockets ****/

int initialisationServeur(char *service,int connexions){

	printf("** initialisation serveur TCP **\n");

	struct addrinfo precisions,*resultat,*origine;
	int statut;
	int s;

	/* Construction de la structure adresse */
	memset(&precisions,0,sizeof precisions);
	precisions.ai_family=AF_UNSPEC;
	precisions.ai_socktype=SOCK_STREAM;
	precisions.ai_flags=AI_PASSIVE;
	statut=getaddrinfo(NULL,service,&precisions,&origine);
	if(statut<0){ perror("initialisationServeur.getaddrinfo"); exit(EXIT_FAILURE); }
	struct addrinfo *p;
	for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
		if(p->ai_family==AF_INET6){ resultat=p; break; }

	/* Creation d'une socket */
	s=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol);
	if(s<0){ perror("initialisationServeur.socket"); exit(EXIT_FAILURE); }

	/* Options utiles */
	int vrai=1;
	if(setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&vrai,sizeof(vrai))<0){
		perror("initialisationServeur.setsockopt (REUSEADDR)");
		exit(EXIT_FAILURE);
		}
	if(setsockopt(s,IPPROTO_TCP,TCP_NODELAY,&vrai,sizeof(vrai))<0){
		perror("initialisationServeur.setsockopt (NODELAY)");
		exit(EXIT_FAILURE);
		}

	/* Specification de l'adresse de la socket */
	statut=bind(s,resultat->ai_addr,resultat->ai_addrlen);
	if(statut<0) return -1;

	/* Liberation de la structure d'informations */
	freeaddrinfo(origine);

	/* Taille de la queue d'attente */
	statut=listen(s,connexions);
	if(statut<0) return -1;

	return s;
}

int boucleServeur(int ecoute,int (*traitement)(int))
	{
	int dialogue;

	while(1){

	    /* Attente d'une connexion */
	    printf("Attente d'une connexion sur la socket %d\n", ecoute);
	    if((dialogue=accept(ecoute,NULL,NULL))<0) return -1;
	    printf("Connecte\n");

	    /* Passage de la socket de dialogue a la fonction de traitement */
	    printf("Debut traitement\n");
	    if(traitement(dialogue)<0){ 
	    	shutdown(ecoute,SHUT_RDWR); 
	    	return 0;
	    }
	}
}

void serveurMessages(char *service,void (*traitement)(unsigned char *,int)){

	param_t * param;
	printf("Initialisation Serveur UDP\n");

	struct addrinfo precisions,*resultat,*origine;
	int statut;


	/* Construction de la structure adresse */
	memset(&precisions,0,sizeof precisions);
	precisions.ai_family=AF_UNSPEC;
	precisions.ai_socktype=SOCK_DGRAM;
	precisions.ai_flags=AI_PASSIVE;
	statut=getaddrinfo(NULL,service,&precisions,&origine);
	if(statut<0){ perror("initialisationSocketUDP.getaddrinfo"); exit(EXIT_FAILURE); }
	struct addrinfo *p;
	for(p=origine,resultat=origine;p!=NULL;p=p->ai_next)
		if(p->ai_family==AF_INET6){ resultat=p; break; }

	printf("Construction de la structure adresse : done\n");

	/* Creation d'une socket */
	socket_udp=socket(resultat->ai_family,resultat->ai_socktype,resultat->ai_protocol);
	if(socket_udp<0){ perror("initialisationSocketUDP.socket"); exit(EXIT_FAILURE); }

	printf("Creation d'une socket = %d sur le port %s: done\n",socket_udp, service);

	/* Options utiles */
	int vrai=1;
	if(setsockopt(socket_udp,SOL_SOCKET,SO_BROADCAST,&vrai,sizeof(vrai))<0){
		perror("initialisationServeurUDPgenerique.setsockopt (REUSEADDR)");
		exit(-1);
	}

	/* Specification de l'adresse de la socket */
	statut=bind(socket_udp,resultat->ai_addr,resultat->ai_addrlen);
	if(statut<0) { perror("initialisationServeurUDP.bind"); exit(-1);}

	/* Debut boucle lecture sur socket */
	printf("Debut boucle lecture sur socket\n");
	while(1)
	{
		struct sockaddr_storage adresse;
		socklen_t taille=sizeof(adresse);
		unsigned char message[MAX_TAMPON];
		int nboctets=recvfrom(socket_udp,message,MAX_TAMPON,0,(struct sockaddr *)&adresse,&taille);
		
		if(nboctets<0)
		{
			printf("Aucun message reçu");
		}
		
		param = malloc(sizeof(param_t));
		param->message = message;
		param->nboctets = nboctets;
		traitement(param->message, param->nboctets);
	}

	/* Liberation de la structure d'informations */
	freeaddrinfo(origine);
}


int envoiMessage(char *service, unsigned char *paquet, int taille_paquet){

	printf("envoiMessage debut, socket_udp = %d\n",socket_udp);

	struct sockaddr_in adresse_broadcast;
	int port = atoi(service);

    memset(&adresse_broadcast, '\0', sizeof(struct sockaddr_in));
    adresse_broadcast.sin_family = AF_INET;
    adresse_broadcast.sin_port = htons(port);
    adresse_broadcast.sin_addr.s_addr = inet_addr(ADRESSE_BROADCAST);

    printf("envoiMessage - sendto\n");

    if(sendto(socket_udp, paquet, taille_paquet, 0, (struct sockaddr *)&adresse_broadcast, sizeof(struct sockaddr_in)) < 0)
    	perror("sendto"); 

    return 0;

}

int envoiMessage_urgent(char *service, unsigned char *paquet, int taille_paquet){

	printf("envoiMessage_urgent debut, socket_udp = %d\n",socket_udp);

	struct sockaddr_in adresse_broadcast;
	int port = atoi(service);

    memset(&adresse_broadcast, '\0', sizeof(struct sockaddr_in));
    adresse_broadcast.sin_family = AF_INET;
    adresse_broadcast.sin_port = htons(port);
    adresse_broadcast.sin_addr.s_addr = inet_addr(ADRESSE_BROADCAST);

    printf("envoiMessage_urgent - sendto\n");

    /* sendto avec flag MSG_OOB pour messages urgents*/
    if(sendto(socket_udp, paquet, taille_paquet, MSG_OOB, (struct sockaddr *)&adresse_broadcast, sizeof(struct sockaddr_in)) < 0)
    	perror("sendto"); 

    return 0;

}


int envoiMessageUnicast(char *service, char *adresse, unsigned char *paquet, int taille_paquet){

	printf("envoiMessageUnicast debut, socket_udp = %d\n",socket_udp);

  	struct sockaddr_in adresse_broadcast;
	int port = atoi(service);

    memset(&adresse_broadcast, '\0', sizeof(struct sockaddr_in));
    adresse_broadcast.sin_family = AF_INET;
    adresse_broadcast.sin_port = htons(port);
    adresse_broadcast.sin_addr.s_addr = inet_addr(adresse);

	printf("envoiMessageUnicast - sendto\n");

  	if(sendto(socket_udp, paquet, taille_paquet, 0, (struct sockaddr *)&adresse_broadcast, sizeof(struct sockaddr_in)) < 0)
    	perror("sendto"); 

    return 0;

}
