/** fichier libcom.h **/

/******************************************************************/
/** Ce fichier decrit les structures et les constantes utilisees **/
/** par les fonctions de gestion des sockets                     **/
/******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>

/**** Constantes ****/

/* nombres de connexions maximums supporté par le serveur TCP */
#define MAX_CONNEXIONS 10 

/* taille max d'un paquet TCP */
#define MAX_LIGNE 1000 

/* taille max d'un paquet UDP */
#define MAX_TAMPON	1024

/* adresse de broadcast */
#define ADRESSE_BROADCAST "127.0.0.1"


/******************************************************************************************************/
/* param_s: Définition de la structure param_s, paramètre de la fonction de traitement du serveur UDP */
/* message : pointe vers les données reçu en UDP */
/* nboctets : taille des données reçu en UDP */
/******************************************************************************************************/

typedef struct param_s { unsigned char * message; int nboctets; } param_t;


/******************************************************************************************************/
/* initialisationServeur : Fonction d'initalisation du serveur TCP */
/* service : pointeur vers le port */
/* connexions : nombres de connexions maximum avec les clients */
/* revoie une socket TCP */
/******************************************************************************************************/

int initialisationServeur(char *service,int connexions);


/******************************************************************************************************/
/* boucleServeur : fonction de gestions des connexions avec les clients */
/* ecoute : socket TCP sur laquelle écouter */
/* traitement : pointe vers la fonction de de traitement des connecions */
/* renvoie -1 si erreur de connexion */
/* renvoie 0 si erreur au niveau de la fonction de traitement ou fin de la connexion */
/******************************************************************************************************/

int boucleServeur(int ecoute,int (*traitement)(int));


/******************************************************************************************************/
/* serveurMessages : initialise un serveur UDP et écoute sur ce même serveur */
/* service : port sur lequel initialiser le serveur UDP */
/* traitement : fonction de traitrement des paquets UDP reçu */ 
/******************************************************************************************************/

void serveurMessages(char *service,void (*traitement)(unsigned char *,int));


/******************************************************************************************************/
/* envoiMessage : fonction d'envoie de paquet UDP en broadcast total */
/* service : port sur lequel envoyer */
/* paquet : chaine de caractère à envoyer */
/* taille_paquet : taille de la chaine de caractère à envoyer */
/* renvoie 0 si tout s'est bien passé, sinon renvoie une erreur */
/******************************************************************************************************/

int envoiMessage(char *service, unsigned char * paquet, int taille_paquet);


/******************************************************************************************************/
/* envoiMessage_urgent : fonction d'envoie de paquet UDP prioritaire en broadcast total */
/* service : port sur lequel envoyer */
/* paquet : chaine de caractère à envoyer */
/* taille_paquet : taille de la chaine de caractère à envoyer */
/* renvoie 0 si tout s'est bien passé, sinon renvoie une erreur */
/******************************************************************************************************/

int envoiMessage_urgent(char *service, unsigned char *paquet, int taille_paquet);


/******************************************************************************************************/
/* envoiMessageUnicast : fonction d'envoie de paquet UDP en unicast */
/* service : port sur lequel envoyer */
/* adresse : adresse du destinataire */
/* paquet : chaine de caractère à envoyer */
/* taille_paquet : taille de la chaine de caractère à envoyer */
/* renvoie 0 si tout s'est bien passé, sinon renvoie une erreur */
/******************************************************************************************************/

int envoiMessageUnicast(char *service, char *adresse, unsigned char *paquet, int taille_paquet);

