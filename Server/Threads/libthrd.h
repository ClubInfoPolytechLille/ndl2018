#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h> 
#include <inttypes.h>

/* Nombre de binomes max */
#define NBBINOME 256

/******************************************************************************************************/
/* lanceThread : Fonction lancement d'une thread en mode détaché */
/* traitement : Pointe vers la fonction de traitement à faire executer par la thread */
/* p : paramètre de la fonction traitement */
/* size : taille de p */
/******************************************************************************************************/

int lanceThread( void (*traitement)(void *) , void * p , int size );
void *thread(void * paramt);


/******************************************************************************************************/
/* initMutex : Initialisation des mutexs du tableau de mutex */
/******************************************************************************************************/

void initMutex();


/******************************************************************************************************/
/* P : verrouillage mutex */
/* tache : numero de la tache à verrouiller */
/* fait un pthread_mutex_lock du mutex correspondant à la case d'indice tache du tableau de mutex */
/* Si tache > 255, lock le mutex correspondant à la lecture du fichier */
/******************************************************************************************************/

void P(int tache);


/******************************************************************************************************/
/* V : déverrouillage mutex */
/* tache : numero de la tache à déverrouiller */
/* fait un pthread_mutex_unlock du mutex correspondant à la case d'indice tache du tableau de mutex */
/* Si tache > 255, unlock le mutex correspondant à la lecture du fichier */
/******************************************************************************************************/

void V(int tache);

