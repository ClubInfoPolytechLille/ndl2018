#include "libthrd.h"

/* Structure passé en paramètre de la thread */
typedef struct Parametre_s { void (*process)(void *); void * p; } Parametre_t;

/* mutex correxpondant au fichier noms */
pthread_mutex_t mutexFichier_noms;

/* tableau de mutexs, chaque mutex correspond à un binome */
pthread_mutex_t mutexs[NBBINOME];

void P(int mut)
{
	if (mut > (NBBINOME-1))
	{
		pthread_mutex_lock(&mutexFichier_noms);
	}
	else pthread_mutex_lock(&mutexs[mut]);
}

void V(int mut)
{
	if (mut > (NBBINOME-1))
	{
		pthread_mutex_unlock(&mutexFichier_noms);
	}
	else pthread_mutex_unlock(&mutexs[mut]);
}

void initMutex()
{
	uint8_t cpt;
	/*pthread_mutexattr_t attr;
	attr = PTHREAD_MUTEX_ERRORCHECK;*/
	for (cpt = 0 ; cpt < NBBINOME ; cpt++)
	{
		pthread_mutex_init(&mutexs[cpt], NULL);
	}
}

void *thread(void * paramt){

	printf("lancement thread\n");

	Parametre_t * arg = paramt;
	arg->process(arg->p);
	free(arg->p);
	free(paramt);

	printf("thread de traitement fini\n");

	return NULL;
	
}
 
int lanceThread( void (*traitement)(void *) , void * p , int size ){

	int retour;
	Parametre_t * param = malloc(sizeof(Parametre_t));
	param->process = traitement;
	param->p = malloc(size);
	memcpy(param->p,p,size);

	pthread_t tid;
	retour = pthread_create(&tid, NULL, thread, param);
	if (retour == 0){
		pthread_detach(tid);
		printf("thread lance\n");
	}
	return retour;
}


