# Je thème

## Introduction

Explication choix techniques    
      -  La présence d'un bouton nous parait superflu, notre interface web se veut simple
      -  Le thème noir s'active de 21h à 5h du matin, ce sont les paramètres de base du mode nuit sur mon ordinateur

Documentation du comportement attendu:
Entre 21h et 5h, les couleurs du fond et du texte sont "inversée", le site passe en mode nuit.
En dehors de cette tranche horaires le site est en mode jours.

## Code Samples

        var d = new Date();
        var n = d.getHours();
        if(n>= 21 || n <=5){backgroundcolor = "#212529";color = "#D3D3D3 ";}
        else{backgroundcolor = "#FFFFFF";color = "#212529 ";}
        document.body.style.color = color;
        document.body.style.background = backgroundcolor;

## Installation

Installation Locale    
      -  Télécharger tout depuis le GIT
      -  Ouvrir index.html   
