//Variables globales
//Global : Qui est considéré dans sa totalité
var lat = "";
var lon = "";
var chute = "";
var api = "f39f7ec63738d03c4dfe479e512485fb"
//4616a66b8a1ccd7a9411d38cbe68380b0429f2fc0fc36053a17668224528785b
//Je sais pas à quoi ça sert mais ça peut être utile
var request = new XMLHttpRequest();
request.open('GET',"../index.html");
request.responseType = 'text';


//Affiche le status
function Status(trame){
	console.log(trame.status);
	document.getElementById("message_status").innerHTML = trame.status;
}

//Rempli le conteneur de messages
function Message(trame){
	console.log(trame.msg);
	if(trame.msg != NULL){
		document.getElementById("Messages").innerHTML += "<div class=\"post-preview\"> <h3 class=\"post-subtitle\">" + trame.msg + "</h3> <p class=\"post-meta\"><i class=\"far fa-clock\"></i> 12H55 <a href:\"#\"> <i class=\"fas fa-map-marker-alt\"></i> " + getcity(trame) + "  </a></p> \
          </div> \
          <hr>"
	}
}

//Récupération du JSON + parsing + appel des fonctions
function LoadData(){
	asstatus = false;
	$.getJSON('data.json', function(data) {
		var jsonData = JSON.parse(JSON.stringify(data));
		for (var i = 0; i <jsonData.tableau.length; i++){
			Message(jsonData.tableau[i]);

			if(jsonData.tableau[0].status != "NULL" && asstatus == false){
				Status(jsonData.tableau[0]);
				console.log(jsonData.tableau[0].lat);
				lat = jsonData.tableau[0].lat;
				lon = jsonData.tableau[0].lon;
				chute = jsonData.tableau[0].chute;
				asstatus = true;
			}
		}
	});

	OpenWeather();
}

//Charge les données au chargement de la page
$(document).ready(function(){
	LoadData();
});

function OpenWeather(){
	$.getJSON("http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=" + api + "&lang=fr"), function(data){
		var jsonData = JSON.parse(JSON.stringify(data));
		document.getElementById("Temperature").innerHTML = jsonData.tableau[0].trame.temperature.value + "°";
		document.getElementById("Humidité").innerHTML = jsonData.tableau[0].trame.humidity.unit + "%";
		document.getElementById("Vent").innerHTML = jsonData.tableau[0].trame.wind.speed.value + "mps";
		document.getElementById("Nuage").innerHTML = jsonData.tableau[0].trame.clouds.value;
		document.getElementById("Location").innerHTML = getcity(jsonData.tableau[0]);
	}
}

function getcity(trame){
	$.getJSON("http://api.openweathermap.org/data/2.5/weather?lat=" + trame.lat + "&lon=" + trame.lon + "&appid=" + api + "&lang=fr"), function(data){
	var jsonData = JSON.parse(JSON.stringify(data));

	return	jsonData.tableau[0].trame.city.name;
	}
}

//Recharge les données toutes les 10 secondes
setInterval(LoadData,10000);
